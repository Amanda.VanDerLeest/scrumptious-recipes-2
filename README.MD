# Scruptious Recipes

SOLO PROJECT:

- Amanda VanDerLeest

## Design

## Monolith Project

Scrumptious project came pre-built from a forked repository.

# Scrumptious Backend Setup

### Project Contribution:

#### All backend code was written by Amanda VanDerLeest. This includes, but is not limited to:

- [recipes/models.py](recipes/models.py)
- [recipes/admin.py](recipes/admin.py)
- [tags/models.py](tags/models.py)
- [tags/admin.py](tags/admin.py)

#### All frontend code was written by Amanda VanDerLeest. This includes, but is not limited to:

- input frontend templates here

## Backend

This project is a relational database written in Python. It consists of an SQL database which was built utilizing the Django framework. The project is a monolith project. The Ingredients table includes foreign key relations to other tables within the recipes database.

### RECIPES APP: Models for Relational Database Tables. SQL created using the Django framework:

#### Recipe Relational Database Table

    Table Columns consist of:
        Recipe Name
        Recipe Author
        Recipe Description
        Recipe Image
        Recipe Created Date & Time
        Recipe Updated Date & Time

        Added a string method to display the name of each instance to make it easier to read in the django server's admin.

        Added a method to get_absolute_url so that upon successful submission of creating a recipe, the browser would bring you to the url of the recipe that was just created.

#### Measure Relational Database Table

    Table Columns consist of:
        Measurement Name
        Measurement Abbreviation

        Added a string method to display the name of each instance to make it easier to read in the django server's admin.

#### FoodItem Relational Database Table

    Table Columns consist of:
        Food Name

        Added a string method to display the name of each instance to make it easier to read in the django server's admin.

#### Ingredient Relational Database Table

    Table Columns consist of:
        Ingredient Amount
        Ingredient Measurement: Foreign Key to the Measure Database Table.
        Ingredient Recipe: Foreign Key to the Recipe Database Table.
        Ingredient Food: Foreign Key to the FoodItem Database Table.

        Added a string method to display the amount, measurement, food item and recipe name of each instance to make it easier to read in the django server's admin.

#### Step Relational Database Table

    Table Columns consist of:
        Recipe: Foreign Key to the Recipe Database Table. In the constraints, I created a related name "steps" property on the Recipe model to be able to access the instances of Steps.
        Order
        Directions
        food_item: Many to Many field

        Added a string method to display the name of the recipe and the step of each instance to make it easier to read in the django server's admin.

### TAGS APP: Models for Relational Database Tables. SQL created using the Django framework:

#### Tag Relational Database Table

    Table Columns consist of:
        Name
        Recipe: ManyToMany relations between tag and recipes.

        Added a string method to display the name of each instance to make it easier to read in the django server's admin.
