from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

try:
    from tags.models import Tag
except Exception:
    Tag = None


class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"


class TagDetailView(DetailView):
    model = Tag
    template_name = "tags/detail.html"


class TagCreateView(CreateView):
    model = Tag
    fields = ["name", "recipes"]
    template_name = "tags/new.html"


class TagUpdateView(UpdateView):
    model = Tag
    fields = ["name", "recipes"]
    template_name = "tags/edit.html"


class TagDeleteView(DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")
